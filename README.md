# Prime

<p>For the moment this script uses an algorithm of the <b>Eratosthenes</b> screen (https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes)</p>

## Complexity
<p><b>Time complexity</b> : n * sqrt(n) </p>

<hr/>

<p>Don't forget to install the <b>"Numba"</b> module to take advantage of the performance gain</p>
<pre>
    pip install numba
</pre>

## Script
### Example

<p>I want to display the first 100 prime numbers.</p>
<pre>
    <code>
        Prime(100, True)
    </code>
</pre>

<p>I want to display how many prime numbers there are among the first 100.</p>
<pre>
    <code>
        Prime(100, False)
    </code>
</pre>

<h2>ToDo</h2>
<ul>
    <li>Create a program in <b>C++</b> with <b>Cuda</b> to speed up the calculation</li>
</ul>

<p>Some documentation</p>
<ul>
    <li>https://developer.nvidia.com/about-cuda</li>
    <li>https://developer.nvidia.com/cuda-toolkit</li>
</ul>
