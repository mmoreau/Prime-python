import os
import sys
import math
import random
from time import time
from numba import jit


@jit(nopython=True, parallel=True)
def Prime(n, show=False):

	""" n * sqrt(n) """

	if n > 1 and n < 7:

		count = 0

		for i in [2, 3, 5]:
			if i <= n:

				if show:
					print(i)

				if not show:
					count += 1

		if not show:
			print(count)


	if n >= 7:

		count = 0

		for i in [2, 3, 5]:

			if not show:
				count += 1

			if show:
				print(i)

		x = 7

		while x <= n:

			if x%5 != 0:

				sqrt = int(math.ceil(math.sqrt(x)))
				i = 3
				lock = True

				while lock and i <= sqrt:
					if x%i == 0:
						lock = False

					i += 2

				if lock:
					
					if show:
						print(x)

					if not show:
						count += 1

			x += 2

		if not show:
			print(count)


start = time()
r = random.randint(2, 10**7)
Prime(r, False)
print(str(round((time()-start), 4))[:-1] + "ms")

if sys.platform.startswith("win32"):
	os.system("pause")
